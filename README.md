# PHP framework

## Contents

* [Contents](#contents)
* [Configs](#configs)
    * [app.php](#app.php)
    * [database.php](#database.php)
* [Query builder](#query-builder)
    * [Table and database select](#table-and-database-select)
        * [Table](#table)
        * [Database](#database)
    * [**SELECT** query](#select-query)
    * [**INSERT** query](#insert-query)
    * [**WHERE**](#where)
    * [**WHERE** nested](#where-nested)
    * [**WHERE** raw](#where-raw)
    * [**WHERE** with JSON](#where-with-json)
    * [**UPDATE** query](#update-query)
    * [**UPDATE** query with JSON](#update-query-with-json)
    * [**DELETE** query](#delete-query)


## Configs

All configs is placed in `/configs/` directory.

### app.php

Example app.php config file:

```
<?php
return [
    "directory" => "/framework",
    "dev_mode" => true,
    "time_zone" => "Europe/Vilnius"
];
```

### database.php

In database file is defined all connection to database details.

#### Database Management System

At this moment only `mysql` is supported, with no plans for other DBMS systems. But you can easily add your own!

Create folder with DBMS name in `core/database` and file in it with same name, but first letter in upper case.

For example:

* Create folder `core/database` named `mongodb`;
* Create file in that folder named `Mongodb.php`;
* Create class with same name as file;
* Class should implement `IDatabase`;
* Change `database.php` config accordingly;
* Create connection code in `connect` method;
* Create query execution logic in `query` method;

And you are done!

File content example:
```
class Mongodb implements IDatabase
{
    private $connection = null;

    public function connect(String $host, String $user, String $password, String $database, String $charset, Int $port)
    {
        //connection code
    }

    public function query(QueryBuilder $queryBuilder, QueryResult $queryResult)
    {
        //query execution code
    }
}
```

#### Example file

```
<?php
return [
    "dbms_name" => "dbvs name",
    "host" => "localhost",
    "username" => "username",
    "password" => "password",
    "database" => "database name",
    "charset" => "utf8",
    "port" => "3306"
];
```

## Query builder

`QueryBuilder` class was inspired by laravel framework.

### Table and database select

#### Table

You can select table in multiple ways. You can first create `QueryBuilder` instance and then select table:
```
DB::query()->table("my_table");
```
Or you can select table creating it:
```
DB::table("my_table");
```

#### Database

Database is only needed when you want to select from other database(not from that which is defined in config). Selection can be done same as with tables. After `QueryBuilder` object creation:
```
DB::query()->database("my_database");
```
Or with one call:
```
DB::database("my_database");
```

### **SELECT** query

SELECT is very simple, you can pass any fields as parameters, which you want to select:

```
DB::table("my_table")->select("id", "name");
```

If you want to select something more complicated you can use `selectRaw` method:

```
DB::table("my_table")->selectRaw("Count(id) as ?", ["number"]);
```

### **INSERT** query

You can insert one row with passing single dimension array:
```
DB::table("my_table")->insert([
    "name" => "Best new name",
    "money" => 94614
]);
```

Or multiple rows with multi dimension array. But parameters count should match!

```
DB::table("my_table")->insert([
    ["name" => "Best name", "money" => 94614, "date" => "CURRENT_TIMESTAMP"],
    ["name" => "Best name 2", "money" => 10000, "date" => "CURRENT_TIMESTAMP"]
]);
```

### **WHERE**

To use WHERE statement you need to call method on query `where` or `orWhere`.
```
$query = DB::table("my_table")->select();

$query->where("id", ">", 0)->where("name", "Ben")->orWhere("name", "!=", "Andrius");
```

This query will be equivalent to SQL query:

```
SELECT * FROM `my_table` WHERE `name` = "Ben" OR `name` != "Andrius;
```

### **WHERE** nested

You can use nested where too!
```
    $query = DB::table("my_table")->select()->whereNested(function($callback){
        $callback->where("name", "Ben")->where("name", "!=", "Andrius");
    })->orWhere("money", ">", 1000);
```

This query will be equivalent to SQL query:

```
SELECT * FROM `my_table` WHERE (`name` = "Ben" AND `name` != "Andrius) OR `money` > 1000;
```
### **WHERE** raw
If normal methods is not enough, you can use `whereRaw`.

```
$query = DB::table("my_table")->select()->whereRaw("`name` => ?", ["Ben"]);
```

### **WHERE** with JSON

If you want to use JSON search, you need to split column name and properties with `->` symbols. Example: `json_field->property1`. And all properties can be splitted with `->` or `.` , thats up to you.

Spliting properties with `->`:
```
$query = DB::table("my_table")->select();

$query->where("json_field->property1->property2", ">", 0)
```

Spliting properties with dots `.`:
```
$query = DB::table("my_table")->select();

$query->where("json_field->property1.property2", "=", 1)
```

### **UPDATE** query

You can update field by passing array object to `update` method.
```
DB::table("my_table")->update([
    "name" => "Best new name",
    "money" => 94614
]);
```

### **UPDATE** query with JSON

You can update single JSON field property if you need to:

```
DB::table("my_table")->update([
    "json_field->property1" => "New value",
    "json_field->property2" => 89
]);
```

### **DELETE** query

Nothing special about delete, just method without parameters.

```
DB::table("my_table")->delete()-where("name", "Andrius");
```