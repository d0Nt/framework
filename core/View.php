<?php
namespace core;

class View
{
    private $loader;
    private $twig;
    private $data = [];
    private $file = "";

    public function __construct()
    {
        $this->loader = new \Twig\Loader\FilesystemLoader('app/views/production');
        $this->twig = new \Twig\Environment($this->loader);

        $extensionClass = "\\app\\views\\TwigExtensions";
        if(class_exists($extensionClass)) {
            $this->twig->addExtension(new $extensionClass());
        }
    }

    public function bindParams(Array $params)
    {
        $this->data = array_merge($this->data, $params);
        return $this;
    }
    
    public function setFileToRender($file)
    {
        if($file != "") {
            $this->file = $file;
        }
        return $this;
    }

    public function render(String $file = "", Array $data = [])
    {
        $this->setFileToRender($file);

        $this->bindParams($data);
        return $this->twig->render($this->file, $this->data);
    }
}