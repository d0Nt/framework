<?php
namespace core\log;

use core\helpers\Config;
use core\http\Request;

class PhpLog extends Log implements iLog
{
    private $file = "-";

    public function setFile($fileName, $line = -1)
    {
        $this->file = "$fileName ($line)";
        return $this;
    }

    public function toArray()
    {
        $data = parent::toArray();
        
        return array_merge($data, ["file" => $this->file]);
    }

    public function push()
    {
        if(Config::loadFile("app")->dev_mode === true) {
            $this->print();
        }
        parent::push();
    }
}