<?php
namespace core\log;

use core\http\Http;

class Log implements iLog
{
    protected $time;
    protected $url;
    protected $code = "-";
    protected $type = "ERROR";
    protected $message = "-";

    public function __construct()
    {
        $this->time = date("Y-m-d H:i:s");
        $this->url = Http::fullUrl();
    }

    public function setTime($time)
    {
        $this->time = $time;
        return $this;
    }

    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    public function toArray()
    {
        return [
            "time" => $this->time,
            "type" => $this->type,
            "url" => $this->url,
            "code" => $this->code,
            "message" => $this->message
        ];
    }

    public function print($asJson = false)
    {
        if($asJson) {
            echo json_encode($this->toArray(), JSON_PRETTY_PRINT);
            return;
        }
        echo "<div class='page-error ".strtolower($this->type)."'>";
        foreach($this->toArray() as $key => $value) {
            echo "$key: $value<br>";
        }
        echo "</div>";
    }

    public function push()
    {
        $name = explode("\\", get_class($this));
        Logger::create(end($name) . "_" . date("Y-m-d") . ".log")
            ->writeLogToFile($this);
    }
}