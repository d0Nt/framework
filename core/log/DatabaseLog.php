<?php
namespace core\log;

use core\helpers\Config;

class DatabaseLog extends Log implements iLog
{
    private $sql = "-";

    public function setSQL(String $sql)
    {
        $this->sql = "".$sql;
        return $this;
    }

    public function toArray()
    {
        $data = parent::toArray();
        
        return array_merge($data, ["sql" => $this->sql]);
    }

    public function push()
    {
        if(Config::loadFile("app")->dev_mode === true) {
            echo json_encode($this->toArray(), JSON_PRETTY_PRINT);
        }
        parent::push();
    }
}