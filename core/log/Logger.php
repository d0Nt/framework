<?php
namespace core\log;

class Logger
{
    private $fileName = "";

    public function __construct(String $fileName)
    {
        $this->fileName = $fileName;
    }


    public function writeLogToFile(iLog $log, $splitter = " || ")
    {
        $this->writeTextToFile(implode($splitter, $log->toArray()));
    }

    public function writeTextToFile(String $content)
    {
        $fileHandle = fopen(__DIR__."/../../logs/".$this->fileName, "a");
        fwrite($fileHandle, $content . "\r\n");

        fclose($fileHandle);
    }

    public static function create(String $fileName)
    {
        return new Logger($fileName);
    }
}