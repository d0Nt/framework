<?php
/**
 * Twig autoloader using only lib content
 */
spl_autoload_register(function ($class) {
    $prefix = 'Twig';
    $base_dir = __DIR__ . '/';
    $len = strlen($prefix);
    if (strncmp($prefix, $class, $len) !== 0) {
        return;
    }
    $relative_class = substr($class, $len);
    $file = $base_dir . str_replace('_', '/', $relative_class) . '.php';
    if (file_exists($file)) {
        require $file;
    }
});