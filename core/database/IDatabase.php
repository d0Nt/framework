<?php
namespace core\database;

use core\database\query\QueryBuilder;
use core\database\query\QueryResult;

interface IDatabase
{
    public function connect(String $host, String $user, String $password, String $database, String $charset, Int $port);
    public function query(QueryBuilder $query, QueryResult $result);
}