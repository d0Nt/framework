<?php
namespace core\database;

use core\database\query\QueryBuilder;

class Pagination
{
    private $totalRows = 0;
    private $results = [];
    private $perPage = 0;
    private $page = 1;

    public function __construct(QueryBuilder $query, $page = 1, $perPage = 25)
    {
        $selectQuery = clone $query;
        $this->countTotalRows($query);
        $this->page = $page;
        $this->perPage = $perPage;

        $this->validateAndUpdatePage();

        $this->results = $selectQuery->limit($this->perPage)->offset(($this->page - 1) * $this->perPage)->run()->getResults();
    }

    private function validateAndUpdatePage()
    {
        if(!is_int($this->page)){
            $this->page = 1;
        }
        if(!is_int($this->perPage)){
            $this->perPage = 25;
        }

        if($this->page < 1) {
            $this->page = 1;
        } else if($this->page > $this->getTotalPageNumber()) {
            $this->page = $this->getTotalPageNumber();
        }
    }

    private function countTotalRows($query)
    {
        $numOfTotalRowsResult = $query->select("COUNT(*) AS num_rows")->limit(0)->offset(0)->run();
        $this->totalRows = $numOfTotalRowsResult->getResults()[0]["num_rows"];
        return $this;
    }

    public function getTotalRows()
    {
        return $this->totalRows;
    }

    public function getTotalPageNumber()
    {
        return intval(ceil($this->getTotalRows()/$this->perPage));
    }

    public function getItemsInPage()
    {
        return $this->results;
    }

    public function getPageNumber()
    {
        return $this->page;
    }

    public function getItemsPerPage()
    {
        return $this->perPage;
    }

    public function toArray()
    {
        return [
            "page" => $this->getPageNumber(),
            "totalPages" => $this->getTotalPageNumber(),
            "perPage" => $this->getItemsPerPage(),
            "items" => $this->getItemsInPage()
        ];
    }
}