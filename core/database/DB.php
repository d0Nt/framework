<?php
namespace core\database;

use core\database\query\QueryBuilder;

class DB
{
    public static function query()
    {
        return new QueryBuilder();
    }

    public static function table($tableName)
    {
        return (new QueryBuilder())->table($tableName);
    }

    public static function database($databaseName)
    {
        return (new QueryBuilder())->database($databaseName);
    }
}