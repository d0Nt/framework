<?php
namespace core\database\query;

use core\database\Database;
use core\Error;

class QueryBuilder
{
    protected $database = null;
    protected $table = null;
    protected $wheres = [];
    protected $queryType = null;
    protected $selectItems = [];
    protected $limit = null;
    protected $orderBy = null;
    protected $groupBy = null;
    protected $havings = [];
    protected $joins = [];
    protected $unions = [];
    protected $inserts = [];
    protected $updates = [];

    public $operators = [
        '=', '<', '>', '<=', '>=', '<>', '!=', '<=>',
        'like', 'like binary', 'not like', 'ilike',
        '&', '|', '^', '<<', '>>',
        'rlike', 'not rlike', 'regexp', 'not regexp',
        '~', '~*', '!~', '!~*', 'similar to',
        'not similar to', 'not ilike', '~~*', '!~~*',
    ];

    public $validQueryTypes = [
        "select", "insert", "delete", "update"
    ];

    public function table(String $table)
    {
        $this->table = $table;
        return $this;
    }

    public function getTable()
    {
        return $this->table;
    }

    public function database(String $database)
    {
        $this->database = $database;
        return $this;
    }

    public function getDatabase()
    {
        return $this->database;
    }

    /**
     * Gets current query type
     */
    public function getQueryType() 
    {
        return $this->queryType;
    }

    protected function setQueryType($type)
    {
        if(!in_array($type, $this->validQueryTypes)) {
            Database::error("Bad query type '$type'.", "Valid types: " . implode($this->validQueryTypes));
            return;
        }
        $this->queryType = $type;
    }

    public function select($selectItems = ["*"])
    {
        $this->setQueryType("select");
        $selectItems = is_array($selectItems) ? $selectItems : func_get_args();

        $this->selectItems = $selectItems;
        return $this;
    }

    public function addSelect($selectItems = ["*"])
    {
        $this->setQueryType("select");
        $selectItems = is_array($selectItems) ? $selectItems : func_get_args();

        $this->selectItems = array_merge($this->selectItems, $selectItems);
    }

    public function selectRaw(String $sql, Array $params)
    {
        $this->setQueryType("select");
        $this->selectItems[] = compact("sql", "params");
        return $this;
    }

    public function getSelectItems()
    {
        if($this->selectItems == null) return [];
        return $this->selectItems;
    }

    public function insert(Array $insertFields)
    {
        $this->setQueryType("insert");
        if(!is_array($insertFields) || empty($insertFields)) {
            Database::error("Insert method requires array.");
            return $this;
        }

        if(array_key_exists(0, $insertFields) && is_array($insertFields[0])) {
            foreach($insertFields as $insert){
                $this->insert($insert);
            }
            return $this;
        }

        if(array_keys($insertFields) === range(0, count($insertFields) - 1)) {
            Database::error("Array should be key => value.");
            return $this;
        }
        $this->inserts[] = $insertFields;
        return $this;
    }

    public function getInserts()
    {
        return $this->inserts;
    }

    public function update(Array $updateFields)
    {
        $this->setQueryType("update");
        if(!is_array($updateFields) || empty($updateFields)) {
            Database::error("Insert method requires array.");
            return $this;
        }

        if(array_keys($updateFields) === range(0, count($updateFields) - 1)) {
            Database::error("Array should be key => value.");
            return $this;
        }
        
        $this->updates = array_merge($updateFields, $this->updates);
        return $this;
    }

    public function getUpdates()
    {
        return $this->updates;
    }

    public function delete()
    {
        $this->setQueryType("delete");
        return $this;
    }

    public function getWheres()
    {
        return $this->wheres;
    }

    /**
     * Add a basic where clause to the query.
     * 
     * @param String $column Column to search in
     * @param String $operator Operator to compare column with value. < > = etc.
     * @param String $value Value of column
     * @param String $value
     * 
     * @return Query
     */
    public function where($column, String $operator = null, String $value = null, String $boolean = "and")
    {
        $type = "Basic";

        if(is_callable($column)){
            return $this->whereNested($column);
        }

        [$value, $operator] = $this->prepareOperatorAndValue(
            $value, $operator, func_num_args() === 2
        );

        if(is_null($value)) {
            return $this->whereNull($column, $value === null, $boolean);
        }

        $this->wheres[] = compact("type", "column", "operator", "value", "boolean");
        return $this;
    }

    protected function prepareOperatorAndValue($value, $operator, $useDefault = false) 
    {
        if($useDefault) {
            return [$operator, '='];
        } else if(!$this->isValidOperator($operator)) {
            Database::error("Query builder error.", "Operator $operator is invalid.");
            return [$value, '='];
        }
        return [$value, $operator];
    }

    protected function isValidOperator($operator) {
        return in_array($operator, $this->operators);
    }

    public function orWhere(String $column, String $operator, String $value = null)
    {
        [$value, $operator] = $this->prepareOperatorAndValue(
            $value, $operator, func_num_args() === 2
        );

        $this->where($column, $operator, $value, "OR");
        return $this;
    }

    protected function whereNull($column, $isNull = true, $boolean = "and")
    {
        $type = $isNull ? "NULL" : "NOT NULL";
        $this->wheres[] = compact("type", "column", "boolean");
        return $this;
    }

    public function whereIsNull($column, $boolean = "")
    {
        $this->whereNull($column, true, $boolean);
        return $this;
    }

    public function whereIsNotNull($column, $boolean = "and")
    {
        $this->whereNull($column, false, $boolean);
        return $this;
    }

    public function whereNested($callback, $boolean = 'and')
    {
        call_user_func($callback, $query = new QueryBuilder());

        if(count($query->getWheres()) > 0)
        {
            $type = "Nested";
            $whereItems = $query->getWheres();
            $this->wheres[] = compact("type", "whereItems", "boolean");
        }
        return $this;
    }

    public function whereRaw(String $sql, Array $params, String $boolean = "and") {
        $type = "Raw";

        if(substr_count($sql, '?') != count($params)) {
            Database::error("Query builder error.", "Bad whereRaw parameters count. Query require: " . 
                substr_count($sql, '?') . ", params: " . count($params) . ". ");
            return $this;
        }

        $this->wheres[] = compact("type", "sql", "params", "boolean");
        return $this;
    }

    public function limit(Int $limit) 
    {
        $offset = 0;
        if($this->limit != null) {
            $offset = $this->limit["offset"];
        }
        $this->limit = compact("limit", "offset");
        return $this;
    }

    public function offset(Int $offset) 
    {
        $limit = 0;
        if($this->limit != null) {
            $limit = $this->limit["limit"];
        }
        $this->limit = compact("limit", "offset");
        return $this;
    }

    public function getLimit() 
    {
        return $this->limit;
    }

    public function orderBy(String $column, String $order="desc")
    {
        $this->orderBy = compact("column", "order");
        return $this;
    }

    public function getOrderBy() 
    {
        return $this->orderBy;
    }

    public function groupBy(String $column)
    {
        $this->groupBy = $column;
        return $this;
    }

    public function getGroupBy() 
    {
        return $this->groupBy;
    }

    public function having($column, $operator, $value, $boolean = "and")
    {
        $type = "Basic";

        [$value, $operator] = $this->prepareOperatorAndValue(
            $value, $operator, func_num_args() === 2
        );
        $this->havings[] = compact("type", "column", "operator", "value", "boolean");
        return $this;
    }

    public function havingRaw(String $sql, Array $params, String $boolean = "and") {
        $type = "Raw";

        if(substr_count($sql, '?') != count($params)) {
            Database::error("Query builder error.", "Bad havingRaw parameters count. Query require: " . 
                substr_count($sql, '?') . ", params: " . count($params) . ". ");
            return $this;
        }

        $this->havings[] = compact("type", "sql", "params", "boolean");
        return $this;
    }

    public function getHavings() 
    {
        return $this->havings;
    }

    public function getJoins()
    {
        return $this->joins;
    }

    /**
     * @var String|Join|null $column
     */
    public function join($table, $column = null, $operator = null, $value = null, $joinType = "inner", $boolean = "and") 
    {
        $join = new Join($joinType, $table);

        if(is_callable($column)) {
            call_user_func($column, $join);
        } else if($column != null) {
            $join->on($column, $operator, $value, $boolean);
        }

        $this->joins[] = $join;
        return $this;
    }

    public function innerJoin($table, $column = null, $operator = null, $value = null, $boolean = "and")
    {
        $this->join($table, $column, $operator, $value, "inner", $boolean);
        return $this;
    }

    public function outerJoin($table, $column = null, $operator = null, $value = null, $boolean = "and")
    {
        $this->join($table, $column, $operator, $value, "outer", $boolean);
        return $this;
    }

    public function leftJoin($table, $column = null, $operator = null, $value = null, $boolean = "and")
    {
        $this->join($table, $column, $operator, $value, "left", $boolean);
        return $this;
    }

    public function rightJoin($table, $column = null, $operator = null, $value = null, $boolean = "and")
    {
        $this->join($table, $column, $operator, $value, "right", $boolean);
        return $this;
    }

    public function crossJoin($table, $column = null, $operator = null, $value = null, $boolean = "and")
    {
        $this->join($table, $column, $operator, $value, "cross", $boolean);
        return $this;
    }

    public function union(QueryBuilder $query, $type = "union")
    {
        if($this->getQueryType() == null) {
            Database::error("Only select query can be in UNION.", "This query type was: " . $this->getQueryType());
            return $this;
        } else if($query->getQueryType() == null) {
            Database::error("Only select query can be in UNION.", "Another query type was: " . $query->getQueryType());
            return $this;
        }
        $this->unions[] = compact("type", "query");
        return $this;
    }

    public function unionAll(QueryBuilder $query)
    {
        $this->union($query, "union all");
        return $this;
    }

    public function getUnions()
    {
        return $this->unions;
    }

    public function run()
    {
        return Database::query($this);
    }
}