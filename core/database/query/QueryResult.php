<?php
namespace core\database\query;

class QueryResult
{
    private $affectedRows = 0;
    private $lastInsertedId = 0;
    private $numRows = 0;
    private $executionTime = -1.0;
    private $results = [];
    private $error = false;

    public function setAffectedRows(Int $affectedRows)
    {
        $this->affectedRows = $affectedRows;
        return $this;
    }

    public function getAffectedRows()
    {
        return $this->affectedRows;
    }

    public function setNumberOfRows(Int $numRows)
    {
        $this->numRows = $numRows;
        return $this;
    }

    public function getNumberOfRows()
    {
        return $this->numRows;
    }

    public function setResults($results)
    {
        $this->results = $results;
        return $this;
    }

    public function getResults()
    {
        return $this->results;
    }

    public function setLastInsertedId($id)
    {
        $this->lastInsertedId = $id;
        return $this;
    }

    public function getLastInsertedId()
    {
        return $this->lastInsertedId;
    }

    public function setExecutionTime(float $time) {
        $this->executionTime = $time;
        return $this;
    }

    public function getExecutionTime() {
        return $this->executionTime;
    }

    public function errorHasOccured()
    {
        $this->error = true;
        return $this;
    }

    public function hasErrorOccured()
    {
        return $this->error;
    }

    public function hasAnyResults()
    {
        return count($this->results) > 0;
    }
}
