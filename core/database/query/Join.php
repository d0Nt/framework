<?php
namespace core\database\query;

use core\database\Database;

class Join
{
    private $type = "inner";
    private $table;
    private $on = [];

    private $joinTypes = [
        "inner", "outer", "left", "right", "cross"
    ];

    public function __construct($type, $table)
    {
        $this->table = $table;
        $this->setType($type);
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        if(!in_array($type, $this->joinTypes)) {
            Database::error("Bad join type '$type'.",
                "Valid types: " . implode(",", $this->joinTypes));
            return $this;
        }
        $this->type = $type;
        return $this;
    }

    public function getTable()
    {
        return $this->table;
    }

    public function on($column, $operator = null, $value = null, $boolean = "and") 
    {
        $type = "Basic";
        $this->on[] = compact("type", "column", "operator", "value", "boolean");
        return $this;
    }

    public function orOn($column, $operator = null, $value) 
    {
        $type = "Basic";
        $boolean = "or";
        $this->on[] = compact("type", "column", "operator", "value", "boolean");
        return $this;
    }

    public function getJoinOn()
    {
        return $this->on;
    }
}