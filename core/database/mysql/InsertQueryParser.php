<?php
namespace core\database\mysql;

class InsertQueryParser extends QueryParser
{
    public function getParsed()
    {
        $insertSQL = "INSERT INTO " . $this->parseDatabaseAndTable();

        $insertArray = $this->query->getInserts();
        $insertSQL .= " (" . implode(", ", array_keys($insertArray[0])) . ") VALUES ";

        return $insertSQL . $this->getInsertValues($insertArray);
    }

    private function getInsertValues($insertArray)
    {
        $values = "";

        foreach($insertArray as $row) {
            if(strlen($values) > 1) $values .= ", ";
            $values .= " (".implode(", ", $this->getInsertRowDataAndBindValues($row)) . ") ";
        }
        
        return $values;
    }

    private function getInsertRowDataAndBindValues($row)
    {
        $valuesArray = array_fill(0, count($row), '?');

        foreach($row as $key => $column) {
            if($this->isValueConstant($column)) {
                $idInArray = array_search($key, array_keys($row));
                $valuesArray[$idInArray] = strtoupper($column);
                continue;
            }
            $this->bindParam($column);
        }

        return $valuesArray;
    }

}