<?php
namespace core\database\mysql;

use core\database\query\QueryBuilder;
use core\helpers\Config;

class QueryParser 
{
    /**
     * @var QueryBuilder $query
     */
    protected $query;

    /**
     * @var Mysql $mysqlConnection
     */
    protected $connection;

    protected $params = [];

    protected $constantValues = [
        "NULL", "CURRENT_TIMESTAMP"
    ];


    public function __construct(QueryBuilder $query, Mysql $connection)
    {
        $this->query = $query;
        $this->connection = $connection;
    }

    public function isValueConstant($value)
    {
        return in_array(strtoupper($value), $this->constantValues);
    }

    public function isJsonField($name, $value) 
    {
        if(strpos($name, "->") !== false)
            return true;
        return false;
    }

    protected function bindParam($params) 
    {
        if(is_array($params)) {
            foreach($params as $param) {
                $this->bindParam($param);
            }
            return;
        }
        $type = "s";
        if(is_int($params) || ctype_digit($params)){
            $type = "i";
        } else if(floatval($params) && intval($params) != $params) {
            $type = "d";
        }
        $value = $params;
        $this->params[] = compact("type", "value");
    }

    protected function parseDatabaseAndTable() 
    {
        $dbAndTable = ""; 
        if($this->query->getDatabase() != null && $this->query->getDatabase() != Config::loadFile("database")->database) {
            $dbAndTable = "`".$this->query->getDatabase()."`.";
        }
        $dbAndTable .= "`".$this->query->getTable()."`";
        return $dbAndTable;
    }

    protected function parseExpressions($expressions) 
    {
        $SQL = "";
        foreach($expressions as $expr) {
            if(strlen($SQL) > 1) {
                $SQL .= " " . strtoupper($expr["boolean"]) . " ";
            }
            if($expr["type"] == "Nested") {
                $SQL .= "(".$this->parseExpressions($expr["whereItems"]).")";

            } else if($expr["type"] == "Raw") {
                $SQL .= $this->parseRawExpressions($expr);

            } else if($expr["type"] == "NULL") {
                $SQL .= "`".trim($expr["column"], '`')."` IS NULL";

            } else if($expr["type"] == "NOT NULL") {
                $SQL .= "`".trim($expr["column"], '`')."` IS NOT NULL";

            } else if($expr["type"] == "Basic") {
                if($this->isJsonField($expr["column"], $expr["value"])) {
                    $SQL .= $this->parseJsonWhereField($expr["column"], $expr["value"], $expr["operator"]);
                    continue;
                }
                $this->bindParam($expr["value"]);
                $SQL .= "`".trim($expr["column"], '`')."` ".$expr["operator"]." ?";
            }
        }

        return $SQL;
    }

    private function parseJsonWhereField($name, $value, $operator) 
    {
        if(strpos($name, "->") !== false) {
            $params = explode('->', $name);
            $columnName = array_shift($params);
            $this->bindParam(implode(".", $params));
            $this->bindParam($value);

            return " JSON_EXTRACT(`$columnName`, CONCAT('$.', ?)) ".$operator." ?";
        }
        return "";
    }

    protected function parseRawExpressions($expression) 
    {
        $this->bindParam($expression["params"]);
        return $expression["sql"];
    }

    public function parseParams()
    {
        $types = "";
        $values = [];
        foreach($this->params as $p) {
            $types .= $p["type"];
            $values[] = $p["value"];
        }
        return compact("types", "values");
    }

    public function getParams()
    {
        return $this->params;
    }
}