<?php
namespace core\database\mysql;

class SelectQueryParser extends QueryParser
{
    private function parseSelectItems() 
    {
        $items = [];
        foreach($this->query->getSelectItems() as $item) {
            if(is_array($item)) {
                foreach($item["params"] as $param) {
                    $param = $this->mysqlConnection->escapeString($param);
                    $item["sql"] = preg_replace("/\?/", $param, $item["sql"], 1);
                }
                $item = $item["sql"];
            }
            $items[] = $item;
        }
        return $items;
    }


    private function parseUnions($unions)
    {
        $sql = "";
        foreach($unions as $union) {
            $sql .= " " . strtoupper($union["type"]) . " ";
            $parser = new SelectQueryParser($union["query"], $this->connection);
            $sql .= $parser->getParsed();
            $this->bindParam($parser->parseParams()["values"]);
        }

        return $sql;
    }

    /**
     * @var QueryJoin[] $joins
     */
    private function parseJoins($joins) {
        $SQL = "";
        foreach($joins as $key => $join) {
            $joinSQL = strtoupper($join->getType()) . " JOIN " . $join->getTable();

            if(!empty($join->getJoinOn())) {
                $joinSQL .= " ON " . $this->parseExpressions($join->getJoinOn());
            }
            
            if($key == 0) {
                $SQL = $joinSQL;
            } else {
                $SQL = " " . $joinSQL;
            }
        }
        return $SQL;
    }

    public function getParsed()
    {
        $selectSQL = "SELECT " . implode(",", $this->parseSelectItems());
        $selectSQL .= " FROM " . $this->parseDatabaseAndTable();

        if(!empty($this->query->getJoins())) {
            $selectSQL .= " " . $this->parseJoins($this->query->getJoins());
        }

        if(!empty($this->query->getWheres())) {
            $selectSQL .= " WHERE " . $this->parseExpressions($this->query->getWheres());
        }

        if($this->query->getGroupBy() != null) {
            $selectSQL .= " GROUP BY " . $this->query->getGroupBy();
        }

        if(!empty($this->query->getHavings())) {
            $selectSQL .= " HAVING " . $this->parseExpressions($this->query->getHavings());
        }

        if($this->query->getOrderBy() != null) {
            $order = $this->query->getOrderBy();
            $selectSQL .= " ORDER BY " . $order["column"] . " " . $order["order"];
        }

        if($this->query->getLimit() != null) {
            $limits = $this->query->getLimit();
            if($limits["limit"] != 0){
                $selectSQL .= " LIMIT " . $limits["limit"];
                if($limits["offset"] != 0){
                    $selectSQL .= " OFFSET " . $limits["offset"];
                }
            }
        }

        if($this->query->getUnions() != null) {
            $selectSQL .= $this->parseUnions($this->query->getUnions());
        }
        return $selectSQL;
    }
}