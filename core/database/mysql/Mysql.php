<?php
namespace core\database\mysql;

use core\database\Database;
use core\database\IDatabase;
use core\database\query\QueryBuilder;
use core\database\query\QueryResult;
use mysqli;

class Mysql implements IDatabase
{
    private $connection = null;

    public function connect(String $host, String $user, String $password, String $database, String $charset, Int $port)
    {
        if($this->connection !== null) return;
        $this->connection = new mysqli($host, $user, $password, $database, $port);
        if ($this->connection->connect_error) {
            Database::error("Database connection failed.", $this->connection->connect_error);
            $this->connection = null;
        }

        $this->connection->set_charset($charset);
    }

    /**
     * 
     * @return bool False if failed, True if successful
     */
    public function query(QueryBuilder $queryBuilder, QueryResult $queryResult)
    {
        if($this->connection === null) {
            Database::error("No open connection to database.", "To execute query you need open connection.");
            $queryResult->errorHasOccured();
            return false;
        }

        if(!$queryBuilder instanceof QueryBuilder) {
            Database::error('Bad query class given', '$queryBuilder need to be instance of class QueryBuilder');
            $queryResult->errorHasOccured();
            return false;
        }

        $query = new MysqlQuery($queryBuilder, $this);
        $preparedStatement = $this->connection->prepare($query->getSQL());
        if($preparedStatement === false) {
            Database::error('Failed to create query.', "".$this->connection->error, $query->getSQL());
            $queryResult->errorHasOccured();
            return false;
        }
        
        $params = $query->getParams();
        if(strlen($params["types"]) > 0) {
            $preparedStatement->bind_param($params["types"], ...$params["values"]);
        }
        if($preparedStatement->execute() === false) {
            Database::error('Failed to execute query.', "Error: ".$this->connection->error.".", $query->getSQL());
            $queryResult->errorHasOccured();
            return false;
        }
        
        $queryResult->setAffectedRows($preparedStatement->affected_rows);

        $result = $preparedStatement->get_result();
        $queryResult->setLastInsertedId($this->connection->insert_id);
        if($result == false){
            return $queryResult;
        }
        $queryResult->setNumberOfRows($result->num_rows);

        if(isset($result->num_rows) && $result->num_rows != 0){
            $queryResult->setResults($result->fetch_all(MYSQLI_ASSOC));
        }
        $preparedStatement->close();
        return $queryResult;
    }

    public function startTransaction($FLAG = MYSQLI_TRANS_START_READ_WRITE)
    {
        if($this->connection === null) {
            Database::error("No open connection to database.", "Transaction requires open connection.");
            return;
        }
        if($FLAG !== MYSQLI_TRANS_START_READ_ONLY && $FLAG !== MYSQLI_TRANS_START_READ_WRITE && $FLAG !== MYSQLI_TRANS_START_WITH_CONSISTENT_SNAPSHOT) {
            Database::error("Bad transaction flag.", "Only mysqli transaction flags possible. Leave blank for default.");
            return;
        }
        $this->connection->begin_transaction($FLAG);
    }

    public function endTransaction()
    {
        $this->commit();
    }

    public function commit()
    {
        if($this->connection === null){
            Database::error("No open connection to database.", "Transaction requires open connection.");
            return;
        }
        $this->connection->commit();
    }

    public function rollback(){
        if($this->connection === null){
            Database::error("No open connection to database.", "Transaction requires open connection.");
            return;
        }
        $this->connection->rollback();
    }

    /**
     * Last inserted row id
     * @return mixed
     */
    public function lastInserted(){
        return self::$connection->insert_id;
    }

    /**
     * Escape string
     * @param $string
     * @return string
     */
    public function escapeString($string){
        if ($this->connection === null)
        Database::error("To escape string you need to open connection first", "");
        return $this->connection->real_escape_string($string);
    }

}