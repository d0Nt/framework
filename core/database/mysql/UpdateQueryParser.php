<?php
namespace core\database\mysql;

class UpdateQueryParser extends QueryParser
{
    public function getParsed()
    {
        $updateSQL = "UPDATE " . $this->parseDatabaseAndTable() . " SET ";

        $updateSQL .= $this->getUpdatesString($this->query->getUpdates());

        if(!empty($this->query->getWheres())) {
            $updateSQL .= " WHERE " . $this->parseExpressions($this->query->getWheres());
        }
       
        return $updateSQL;
    }

    private function parseJsonUpdateField($name, $value) 
    {
        if(strpos($name, "->") !== false) {
            $params = explode('->', $name);
            $columnName = array_shift($params);
            $this->bindParam(implode(".", $params));
            $this->bindParam($value);

            return "`$columnName` = JSON_REPLACE(`$columnName`, CONCAT('$.', ?), ?)";
        }
        return "";
    }

    private function getUpdatesString($updates)
    {
        $sql = "";
        foreach($updates as $key => $value) {
            if(strlen($sql) > 1) $sql .= ", ";

            if($this->isJsonField($key, $value)){
                $sql.= $this->parseJsonUpdateField($key, $value);
                continue;
            }

            if($this->isValueConstant($value)) {
                $sql .= "`$key` = ".strtoupper($value);
                continue;
            }

            $sql .= "`$key` = ?";
            $this->bindParam($value);
        }

        return $sql;
    }
}