<?php
namespace core\database\mysql;

use core\database\Database;
use core\database\query\QueryBuilder;

class MysqlQuery
{
    private $query;
    private $connection;

    private $sql = "";
    private $params = "";


    public function __construct(QueryBuilder $query, Mysql $connection)
    {
        $this->query = $query;
        $this->connection = $connection;
        $this->parseSQL();
    }

    public function getParams()
    {
        return $this->params;
    }

    public function getSQL()
    {
        return $this->sql;
    }

    private function parseSQL()
    {
        $parser = null;
        if($this->query->getQueryType() == null){
            Database::error("Query type was not specified.", "Valid types: select, insert, delete, update");
            return;
        }

        if($this->query->getQueryType() == "select") {
            $parser = new SelectQueryParser($this->query, $this->connection);
        } else if($this->query->getQueryType() == "insert") {
            $parser = new InsertQueryParser($this->query, $this->connection);
        } else if($this->query->getQueryType() == "delete") {
            $parser = new DeleteQueryParser($this->query, $this->connection);
        } else if($this->query->getQueryType() == "update") {
            $parser = new UpdateQueryParser($this->query, $this->connection);
        }

        $this->sql = $parser->getParsed();
        $this->params = $parser->parseParams();
    }
}