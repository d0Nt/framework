<?php
namespace core\database\mysql;

class DeleteQueryParser extends QueryParser
{
    public function getParsed()
    {
        $deleteSQL = "DELETE FROM " . $this->parseDatabaseAndTable();

        if(!empty($this->query->getWheres())) {
            $deleteSQL .= " WHERE " . $this->parseExpressions($this->query->getWheres());
        }

        return $deleteSQL;
    }
}