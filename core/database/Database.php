<?php
namespace core\database;

use core\database\query\QueryBuilder;
use core\database\query\QueryResult;
use core\helpers\Config;
use core\log\DatabaseLog;
use Exception;

class Database
{
    /**
     * @var IDatabase   $connection
     */
    private static $connection = null;

    private static function connect()
    {
        if(self::$connection != null) return;
        $config = Config::loadFile("database");

        $dbvsClass = "\\core\\database\\".$config->dbms_name."\\".ucfirst($config->dbms_name);

        if(!class_exists($dbvsClass)){
            throw new Exception("$config->dbms_name is not supported.");
            return;
        }
        self::$connection = new $dbvsClass();
        self::$connection->connect($config->host, $config->username, $config->password, $config->database, $config->charset, $config->port);
    }

    public static function error($safeDescription, $fullDescription = "", $query = "") 
    {
        (new DatabaseLog())
            ->setSQL($query)
            ->setMessage($fullDescription)
            ->setCode($safeDescription);
        die();
    }

    /**
     * @return QueryResult
     */
    public static function query(QueryBuilder $query)
    {
        self::connect();
        $result = new QueryResult();
        $started = microtime(true);
        self::$connection->query($query, $result);
        $result->setExecutionTime(microtime(true)-$started);
        return $result;
    }
}