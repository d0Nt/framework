<?php

use core\Controller;
use core\Helpers\Config;
use core\log\PhpLog;
use core\Redirect;
use core\routes\Router;

$config = Config::loadFile("app");

function shutdown()
{
    global $config;
    
    $error = error_get_last();
    if($error != null) {
        (new PhpLog())
            ->setFile($error["file"], $error["line"])
            ->setCode($error["type"])
            ->setMessage($error["message"])
            ->push();

        if($config->dev_mode == false && Controller::isValidController("error")){
            Redirect::appDir("error/server");
        }
    }
}

register_shutdown_function('shutdown');

ini_set('display_errors', 0);
error_reporting(E_ALL);

if(property_exists($config, "time_zone")){
    date_default_timezone_set($config->time_zone);
}

require("routes/Routes.php");

Router::proceed();