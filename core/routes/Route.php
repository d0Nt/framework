<?php
namespace core\routes;

use core\http\HttpMethods;
use core\security\InputParser;

class Route
{
    private $method;
    private $url;
    private $type;

    private $pattern;
    private $where = [];
    private $defaultRegex = "[\w\-.]+";

    public function __construct(Int $type, String $url, Object $method)
    {
        $this->url = $url;
        $this->method = $method;
        $this->type = $type;
    }

    public function where(Array $array)
    {
        $this->where = $array;
    }

    public function isDynamic()
    {
        return preg_match('/\{([\w|\-]+)\}/', $this->url) !== 0;
    }

    public function getRegex()
    {
        $this->createRegexFromUrl();
        return $this->pattern;
    }

    public function hasFoundStaticRoute(Int $type, String $url)
    {
        if($type != $this->type) return false;
        if($this->isDynamic()) return false;
        if(strcmp($this->url, $url) === 0)
            return true;
        return false;
    }

    public function hasFoundDynamicRoute(Int $type, String $url)
    {
        if($type == HttpMethods::GET || $type == HttpMethods::DELETE) {
            preg_match($this->getRegex(), $url, $matches, PREG_OFFSET_CAPTURE);
            if(count($matches) > 0) return true; 
        } else {
            parse_str(file_get_contents("php://input"), $bodyVariables);
            preg_match_all("/\{([\w|\-]+)\}/", $this->url, $matches);

            foreach($matches[1] as $match) {
                if(!array_key_exists($match, $bodyVariables))
                    return false;

                if(preg_match($this->getRegexForVariable($match), $bodyVariables[$match]) == 0)
                    return false;
            }
            return true;
        }
    }

    private function createRegexFromUrl()
    {
        $value = str_replace("/", "\/", $this->url);

        $value = preg_replace_callback("/\{([\w|\-]+)\}/", 
            function($matches) {
                foreach($matches as $match) {
                    return $this->getRegexForVariable(trim($match, "{}"));
                }
        }, $value);

        $this->pattern = "/".$value."/";
    }

    private function getRegexForVariable(String $name)
    {
        $regex = $this->defaultRegex;

        if(array_key_exists($name, $this->where)) {
            $regex = $this->where[$name];
        }

        return "(".$regex.")";
    }

    public function executeRoute(String $url)
    {
        if(!$this->isDynamic()){
            $value = ($this->method)();
        } else {
            $arguments = [];
            if($this->type == HttpMethods::GET || $this->type == HttpMethods::DELETE) {
                preg_match($this->getRegex(), $url, $matches, PREG_OFFSET_CAPTURE);
                for($i = 1; $i < count($matches); $i++) {
                    array_push($arguments, $matches[$i][0]);
                }
            } else {
                $values = (new InputParser())->getUnsafeValues();
                preg_match_all("/\{([\w|\-]+)\}/", $this->url, $matches);

                foreach($matches[1] as $match){
                    if(array_key_exists($match, $values)) {
                        $arguments[$match] = $values[$match];
                    }
                }
            }
            $value = call_user_func_array($this->method, $arguments);
        }
        if($value === false) 
            return false;

        echo $value;
        return true;
    }
}