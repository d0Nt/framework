<?php
namespace core\routes;

use core\Controller;
use core\http\Http;
use core\http\HttpMethods;
use core\MethodInfo;

class Router {
    private static $staticRoutes = [];
    private static $dynamicRoutes = [];


    public static function get(String $pattern, Object $function)
    {
        return self::addRoute(HttpMethods::GET, $pattern, $function);
    }

    public static function post(String $pattern, Object $function)
    {
        return self::addRoute(HttpMethods::POST, $pattern, $function);
    }

    public static function put(String $pattern, Object $function)
    {
        return self::addRoute(HttpMethods::PUT, $pattern, $function);
    }

    public static function delete(String $pattern, Object $function)
    {
        return self::addRoute(HttpMethods::DELETE, $pattern, $function);
    }

    private static function addRoute(Int $requestType, String $pattern, Object $_function)
    {
        $route = new Route($requestType, $pattern, $_function);

        if($route->isDynamic()) {
            array_push(self::$dynamicRoutes, $route);
        } else {
            array_push(self::$staticRoutes, $route);
        }
        return $route;
    }


    public static function proceed()
    {
        if(self::proceedStaticRoutes())
            return true;
            
        if(self::proceedDynamicRoutes())
            return true;

        if(self::proceedControllers())
            return true;

        return false;
    }

    private static function proceedStaticRoutes()
    {
        foreach (self::$staticRoutes as $route) {
            if($route->hasFoundStaticRoute(Http::type(), Http::url())) {
                $route->executeRoute(Http::url());
                return true;
            }
        }
        return false;
    }

    private static function proceedDynamicRoutes()
    {
        foreach (self::$dynamicRoutes as $route) {
            if($route->hasFoundDynamicRoute(Http::type(), Http::url())) {
                $route->executeRoute(Http::url());
                return true;
            }
        }
        return false;
    }

    private static function proceedControllers()
    {
        $arguments = [];
        $url = trim(Http::url(), "/");

        $controllerName = Controller::getValidControllerFromUrl($url);
        if($controllerName === false) {
            $controllerName = "index";
        } else {
            $url = trim(substr($url, strlen($controllerName)), "/");
        }
        if(strlen($url) < 1) $url="index";
        $urlParts = explode("/", $url);

        $functionName = "";
        if(count($urlParts) < 1 || !Controller::hasPublicMethod($controllerName, $urlParts[0])) {
            $functionName = "notFound";
        } else {
            $functionName = $urlParts[0];
            array_shift($urlParts);
            $arguments = $urlParts;
        }

        self::executeControllerRoute($controllerName, $functionName, $arguments);
        return false;
    }

    private static function executeControllerRoute($controller, $function, $arguments)
    {
        $controllerClass = Controller::controllerClass($controller);
        $paramCount = MethodInfo::getClassFunctionParametersCount($controllerClass, $function);

        if($paramCount > count($arguments)) {
            $function = "notFound";
            $paramCount = 0;
        }

        $controller = (new $controllerClass());
        if($controller->canAccess()) {
            if($paramCount > 0) {
                $content = call_user_func_array(array($controller, $function), $arguments);
            } else {
                $content = $controller->$function();
            }
        } else {
            $content = $controller->noAccess();
        }

        echo $content;
    }

}