<?php
namespace core;

use core\helpers\Config;

class Redirect
{
    public static function home()
    {
        header('Location: '.Config::loadFile("app")->directory);
    }
    
    public static function appDir($route)
    {
        header('Location: '.Config::loadFile("app")->directory.'/'.$route);
    }

}