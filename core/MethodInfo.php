<?php
namespace core;

class MethodInfo{

    public static function isStatic($class, $method)
    {
        try {
            $res = new \ReflectionMethod($class, $method);
            return $res->isStatic();
        } catch (\ReflectionException $exception) {
            return false;
        }
    }

    public static function isPublic($class, $method)
    {
        try {
            $res = new \ReflectionMethod($class, $method);
            return $res->isPublic();
        } catch (\ReflectionException $exception) {
            return false;
        }
    }

    /**
     * Get class function parameters count
     * @param $class
     * @param $func
     * @return int
     */
    public static function getClassFunctionParametersCount($class, $func)
    {
        try {
            $reflection = new \ReflectionMethod($class, $func);
            return $reflection->getNumberOfParameters();
        }catch (\ReflectionException $exception) {
            return 0;
        }
    }

    /**
     * Get function parameters count
     * @param $func
     * @return int
     */
    public static function getFunctionParametersCount($func)
    {
        try {
            $reflection = new \ReflectionFunction($func);
            return $reflection->getNumberOfParameters();
        }catch (\ReflectionException $exception) {
            return 0;
        }
    }
}