<?php
namespace core\security;

class Hashing
{
    public static function md5($data, $salt="")
    {
        return hash('md5', $data.''.$salt);
    }

    public static function sha256($data, $salt="")
    {
        return hash('sha256', $data.''.$salt);
    }

    private static function randomToken($length = 32)
    {
        if(!isset($length) || intval($length) <= 8 ){
          $length = 32;
        }
        if (function_exists('random_bytes')) {
            return bin2hex(random_bytes($length));
        }
        if (function_exists('openssl_random_pseudo_bytes')) {
            return bin2hex(openssl_random_pseudo_bytes($length));
        }
    }
    
    public static function salt($length = 64)
    {
        return substr(strtr(base64_encode(hex2bin(self::RandomToken(46))), '+', '.'), 0, $length);
    }
}