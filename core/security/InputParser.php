<?php
namespace core\security;

class InputParser
{
    private $parsedValues = null;
    
    public function __construct()
    {
        parse_str(file_get_contents("php://input"), $this->parsedValues);
    }

    public function getUnsafeValues()
    {
        return $this->parsedValues;
    }

    public function __get($name)
    {
        if(array_key_exists($name, $this->getUnsafeValues())) {
            return false;
        }
        return static::safeText($this->parsedValues[$name]);
    }

     /**
     * Escape shellcmd shellargs and html
     * @param $text
     * @return mixed
     */
    public static function safeText($text){
        if(is_array($text)){
            foreach ($text as $key=>$value){
                $text[$key] = self::safeText($value);
            }
            return $text;
        }
        $text = htmlspecialchars_decode($text);
        $text = strip_tags($text);
        return $text;
    }

}