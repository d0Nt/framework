<?php
namespace core\http;

use core\helpers\Config;
use core\security\InputParser;

class Http
{
    public static function type()
    {
        return HttpMethods::getRquestMethod();
    }

    public static function data()
    {
        return new InputParser();
    }

    public static function url()
    {
        return rtrim(str_replace(Config::loadFile("app")->directory, "", parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH)), "/");
    }

    public static function fullUrl()
    {
        return (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";;
    }
}