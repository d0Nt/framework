<?php
namespace core\http;

use core\BaseEnum;

class HttpMethods extends BaseEnum
{
    public const GET = 0;
    public const POST = 1;
    public const DELETE = 2;
    public const PUT = 3;

    public static function getRquestMethod()
    {
        switch($_SERVER['REQUEST_METHOD']) {
            case "POST":
                return self::POST;
            case "GET":
                return  self::GET;
            case "DELETE":
                return  self::DELETE;
            case "PUT":
                return  self::PUT;
        }
    }
}