<?php
namespace core\http;

class Response
{
    public static function setHeader($header, $value)
    {
        header($header . ': ' . $value);
    }

    public static function setContentType($value)
    {
        static::setHeader("Content-Type", $value);
    }

    public static function contentTypeByFile($file)
    {
        switch($file) {
            case '.css':
                return "text/css";
                break;
            case '.jpg':
            case '.jpeg':
                return "image/jpeg";
                break;
            case '.png':
                return "image/png";
                break;
            case '.js':
                return "text/javascript";
                break;
            case '.json':
                return "application/json";
                break;
            default:
                return "text/plain";
                break;
        }
    }
}