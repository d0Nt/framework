<?php
namespace core\http;

use function curl_close;
use function curl_exec;
use function curl_init;
use function curl_setopt;

class Request 
{
    /***
     * @param $url
     * @param array $params
     * @return bool|string
     */
    public static function Post($url, array $params)
    {
        $query = json_encode($params);
        $ch    = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Connection: Keep-Alive'
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
        curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        return $http_code;
    }
}