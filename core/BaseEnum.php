<?php
namespace core;

use ReflectionClass;

class BaseEnum
{
    private static $constCacheArray = [];

    public static function getConstants() 
    {
        $calledClass = get_called_class();
        if (!array_key_exists($calledClass, static::$constCacheArray)) {
            $reflect = new ReflectionClass($calledClass);
            static::$constCacheArray[$calledClass] = $reflect->getConstants();
        }
        return static::$constCacheArray[$calledClass];
    }

    public static function isValidName($name) 
    {
        $constants = static::getConstants();

        return array_key_exists($name, $constants);
    }

    public static function isValidValue($value, $strict = true) 
    {
        $values = array_values(static::getConstants());
        return in_array($value, $values, $strict);
    }

    public static function __callStatic($name, $arguments)
    {
        $array = static::getConstants();
        if(static::isValidName($name)) {
            return new static($array[$name]);
        }
    }
}
