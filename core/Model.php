<?php
namespace core;

use core\database\DB;
use core\database\query\QueryResult;
use Exception;

class Model
{
    /**
     * @var $database Model database
     */
    protected static $database = null;

    /**
     * @var String $database Model table in database
     */
    protected static $table = "";

    /**
     * @var $database Model primary key column name
     */
    protected static $primaryKey = "id";

    protected static $columns = [];

    protected static $ignoreOnSave = [];

    protected $fields = [];

    private $db_values = [];

    public function __construct($primaryKey = null)
    {
        $this->initFields();
        if($primaryKey == null) return;
        $this->fields[static::$primaryKey] = $primaryKey;

        $this->refresh();
    }

    private function initFields()
    {
        foreach(static::$columns as $column) {
            $this->fields[$column] = null;
        }
    }

    public function __get($name)
    {
        return $this->fields[$name];
    }

    public function __set($name, $value)
    {
        $this->fields[$name] = $value;
    }

    public function refresh()
    {
        $result = DB::database(static::$database)
            ->table(static::$table)
            ->select(static::$columns)
            ->where(static::$primaryKey, $this->fields[static::$primaryKey])
            ->limit(1)
            ->run();

        $this->updateFromResult($result);
        return $this;
    }

    protected function updateFromResult(QueryResult $result, Int $resultId = 0) 
    {
        if($result->getNumberOfRows() != 1) return;
        $data = $result->getResults()[$resultId];

        $this->updateFromArray($data);
        return $this;
    }

    protected function updateFromArray(Array $data)
    {
        foreach($data as $key => $value) {
            $this->db_values[$key] = $value;
        }

        $this->fields = $this->db_values;
        return $this;
    }

    public function insert($withPrimaryKey = false)
    {
        if($this->fields[static::$primaryKey] == null && $withPrimaryKey) {
            throw new Exception("Primary key for model '".get_class($this)."' was not set");
            return;
        }
        $fieldsToInsert = [];

        if($withPrimaryKey) {
            $fieldsToInsert[static::$primaryKey] = $this->fields[static::$primaryKey];
        }

        $fieldsToInsert[] = $this->getDataToSave();

        $result = DB::database(static::$database)
            ->table(static::$table)
            ->insert($fieldsToInsert)
            ->run();

        if($result->getAffectedRows() == 1) {
            $this->fields[static::$primaryKey] = $result->getLastInsertedId();
        }
        return $this;
    }

    private function getDataToSave() 
    {
        $fieldsToSave = [];

        foreach(static::$columns as $column) {
            if(in_array($column, static::$ignoreOnSave) || $column == static::$primaryKey) {
                continue;
            }

            if(!in_array($column, array_keys($this->db_values)) || $this->db_values[$column] !== $this->fields[$column]) {
                $fieldsToSave[$column] = $this->fields[$column];
            }
        }
        
        return $fieldsToSave;
    }

    public function save()
    {
        if($this->fields[static::$primaryKey] === null) {
            throw new Exception("Primary key for model '".get_class($this)."' was not set");
            return;
        }

        $this->update($this->getDataToSave());
        return $this;
    }

    public function update(array $fields)
    {
        DB::database(static::$database)
            ->table(static::$table)
            ->update($fields)
            ->where(static::$primaryKey, $this->fields[static::$primaryKey])
            ->run();

        foreach($fields as $key => $value) {
            $this->fields[$key] = $value;
            $this->db_values[$key] = $value;
        }
        return $this;
    }

    public function delete()
    {
        DB::database(static::$database)
            ->table(static::$table)
            ->delete()
            ->where(static::$primaryKey, $this->fields[static::$primaryKey])
            ->run();
        $this->db_values = [];
        $this->fields[static::$primaryKey] = null;
        return $this;
    }

    public function isEmpty()
    {
        foreach($this->fields as $value){
            if($value !== null) return false;
        }
        return true;
    }

    public function inDatabase()
    {
        return $this->fields[static::$primaryKey] !== null;
    }

    /**
     * static methods
     */

     /**
      * @return Model[]
      */
     public static function all()
     {
        $className=get_called_class();

        $result = DB::database(static::$database)
        ->table(static::$table)
        ->select(static::$columns)
        ->run();

        $models = [];
        foreach($result->getResults() as $result)
        {
            $model = new $className();
            $model->updateFromArray($result);
            $models[] = $model;
        }

        return $models;
     }

     public static function where($column, String $operator = null, String $value = null)
     {
        $className=get_called_class();

        $result = DB::database(static::$database)
        ->table(static::$table)
        ->select(static::$columns)
        ->where($column, $operator, $value)
        ->run();

        $models = [];
        foreach($result->getResults() as $result)
        {
            $model = new $className();
            $model->updateFromArray($result);
            $models[] = $model; 
        }
        
        return $models;
     }

     public static function getSelectQuery()
     {
        return DB::database(static::$database)
        ->table(static::$table)
        ->select(static::$columns);
     }
}