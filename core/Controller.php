<?php
namespace core;

class Controller 
{
    private $view = null;

    /**
     * Controller index page
     */
    public function index()
    {
        Redirect::home();
    }

    /**
     * Access restriction to all controller
     * @return bool
     */
    public function canAccess()
    {
        return true;
    }

    /**
     * Error when access rejected
     */
    public function noAccess()
    {
        return "no access";
    }

    /**
     * Not found controller route
     */
    public function notFound()
    {
        return "404 not found";
    }

    public function __get($name)
    {
        if($name == "view") {
            return $this->view();
        }
    }

    /**
     * Get view class
     */
    protected function view($fileName = null, $params = [])
    {
        if($this->view == null) {
            $this->view = new View();
        }

        if($fileName != null) {
            $this->view->setFileToRender($fileName);
        }

        $this->view->bindParams($params);
        return $this->view;
    }

    /**
     * Get full path to controller
     */
    public static function controllerClass(String $controller)
    {
        $controller = str_replace("/", "\\", $controller);
        return "\\app\\controllers\\".$controller."Controller";
    }

    /**
     * Check if controller is valid
     */
    public static function isValidController(String $controller)
    {
        $file = __DIR__. "/../app/controllers/" . $controller . "Controller.php";
        if(!file_exists($file)) return false;

        $className = self::controllerClass($controller);
        if (class_exists($className) && is_subclass_of($className, "core\\Controller")){
            return true;
        }
        return false;
    }

    public static function hasPublicMethod(String $controller, String $method)
    {
        $className = self::controllerClass($controller);

        if(!method_exists($className, $method)) return false;
        if(!MethodInfo::isPublic($className, $method)) return false;
        if(MethodInfo::isStatic($className, $method)) return false;
        return true;
    }

    public static function getValidControllerFromUrl($url)
    {
        $urlParts = explode("/", trim($url, "/"));
        if(end($urlParts) !== "index") array_push($urlParts, "index");

        $lastRoute = "";
        for($i = 0; $i < count($urlParts) - 1; $i++) {
            $lastRoute = trim($lastRoute."/".$urlParts[$i], "/");

            if(!self::isValidController($lastRoute)) continue;
            if(!self::hasPublicMethod($lastRoute, $urlParts[$i+1])) continue;

            return $lastRoute;
        }
        return false;
    }
}