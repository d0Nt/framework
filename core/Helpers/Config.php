<?php
namespace core\helpers;

class Config
{
    public static function loadFile($name)
    {
        $dir = __DIR__ . '/../../configs/' . $name . ".php";
        if(!file_exists($dir))
            return false;
            
        return (object)require $dir;
    }

}