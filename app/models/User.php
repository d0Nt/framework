<?php
namespace app\models;

use core\Model;

class User extends Model
{
    protected static $database = "bots";
    protected static $table = "users";
    protected static $columns = ["id", "name", "password", "money", "date"];
    protected static $ignoreOnSave = ["password"];
}