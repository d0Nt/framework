<?php
namespace app\controllers;

use core\Controller;
use core\log\PhpLog;

class errorController extends Controller 
{
    public function index()
    {
        $this->view('index.twig', ['ez' => 'Fabien']);
        return $this->view->render();
    }

    public function server()
    {
        $this->view('index.twig', ['ez' => 'error']);
        return $this->view->render();
    }
}