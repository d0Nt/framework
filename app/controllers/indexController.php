<?php
namespace app\controllers;

use core\Controller;

class indexController extends Controller 
{
    public function index()
    {
        $this->view('index.twig', ['ez' => 'Fabien']);
        return $this->view->render();
    }
}