<?php
namespace app\controllers\api;

use core\Controller;

class userController extends Controller 
{
    public function index()
    {
        return "User index api page";
    }
    
    public function login()
    {
        return "Login api method";
    }
}