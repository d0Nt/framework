<?php
return [
    "directory" => "/framework",
    "dev_mode" => true,
    "time_zone" => "Europe/Vilnius"
];